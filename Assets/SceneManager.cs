using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManager : MonoBehaviour
{
    public static SceneManager instance;

    public GameObject RenderQuad;
    public GameObject ItemsContainer;

    public GameObject BaseItem;
    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
    }
    public void AddItem(int posX, int posZ)
    {
        BaseItemScript instance = Utilities.CreateInstance(this.BaseItem, this.ItemsContainer, true).GetComponent<BaseItemScript>() ;

        instance.transform.localPosition = new Vector3(posX, 0, posZ);
    }
}
