using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utilities : MonoBehaviour
{
    public static GameObject CreateInstance(GameObject original, GameObject parent, bool isActive)
    {
        GameObject instance = MonoBehaviour.Instantiate(original, parent.transform, false);
        instance.SetActive(isActive);
        return instance;
    }

    public static RenderQuadScript CreateRenderQuad()
    {
        return CreateInstance(SceneManager.instance.RenderQuad, SceneManager.instance.ItemsContainer, true).GetComponent<RenderQuadScript>();
    }
}
