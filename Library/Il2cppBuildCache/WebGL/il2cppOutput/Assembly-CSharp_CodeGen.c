﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void CameraManager::Start()
extern void CameraManager_Start_m3A8F66B836B9A9BC419B62EF88451549BBC9CCD0 (void);
// 0x00000002 System.Void CameraManager::Update()
extern void CameraManager_Update_m911D2029A80BF20148CD94D496452F232343FF60 (void);
// 0x00000003 System.Void CameraManager::.ctor()
extern void CameraManager__ctor_m932EDBCBAE89115380AA929ED5BC9823B9371185 (void);
// 0x00000004 System.Void RenderQuadScript::.ctor()
extern void RenderQuadScript__ctor_mCAD65F9788F7DC64CDA65DB90713D5E111BCF086 (void);
// 0x00000005 System.Void SceneManager::Awake()
extern void SceneManager_Awake_m913FE5962F5FB4BFA11D0ED31212C66272B535E4 (void);
// 0x00000006 System.Void SceneManager::AddItem(System.Int32,System.Int32)
extern void SceneManager_AddItem_m2EC2EF47336C47D92383227D2A8940A6547AC1EE (void);
// 0x00000007 System.Void SceneManager::.ctor()
extern void SceneManager__ctor_m50993E4C2BC958CFCF84678FE3A8304E1CF795BC (void);
// 0x00000008 System.Void BaseItemRenderScript::Start()
extern void BaseItemRenderScript_Start_m72849706427F1D143ABBA0C8A05749F38D742810 (void);
// 0x00000009 System.Void BaseItemRenderScript::Update()
extern void BaseItemRenderScript_Update_mB3A9D29E6E1D43343BCF052E97D4A98A7C87E25E (void);
// 0x0000000A System.Void BaseItemRenderScript::Refresh(System.Int32)
extern void BaseItemRenderScript_Refresh_m877A009F63B00372E11DE6CA073B9F12AF256B54 (void);
// 0x0000000B System.Void BaseItemRenderScript::UpdateRenderQuads(System.Int32)
extern void BaseItemRenderScript_UpdateRenderQuads_mD2A48EB5EA42A0153B300148A848ACA2383D1BC5 (void);
// 0x0000000C System.Void BaseItemRenderScript::.ctor()
extern void BaseItemRenderScript__ctor_mA055442301A5DFD64A2897876BB449D9CB507648 (void);
// 0x0000000D System.Void BaseItemScript::Start()
extern void BaseItemScript_Start_m758933F1DECDB818C38180DAEC85C29A793E9435 (void);
// 0x0000000E System.Collections.IEnumerator BaseItemScript::BaseItemAnimate()
extern void BaseItemScript_BaseItemAnimate_m20AC0862D30741979D9369B02F446132A2DBB0F5 (void);
// 0x0000000F System.Void BaseItemScript::.ctor()
extern void BaseItemScript__ctor_m239BA9844D8CE5A59EB8352D7736F3D96FD15E90 (void);
// 0x00000010 System.Void BaseItemScript/<BaseItemAnimate>d__2::.ctor(System.Int32)
extern void U3CBaseItemAnimateU3Ed__2__ctor_mF437DE2BCFA82602EB99858C6E20DB8A7D943247 (void);
// 0x00000011 System.Void BaseItemScript/<BaseItemAnimate>d__2::System.IDisposable.Dispose()
extern void U3CBaseItemAnimateU3Ed__2_System_IDisposable_Dispose_m1E960114DE81435DAC4FC0FAFC301D82F0394D9F (void);
// 0x00000012 System.Boolean BaseItemScript/<BaseItemAnimate>d__2::MoveNext()
extern void U3CBaseItemAnimateU3Ed__2_MoveNext_m6C90236A022D18B5F71181DC6784B81421383967 (void);
// 0x00000013 System.Object BaseItemScript/<BaseItemAnimate>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBaseItemAnimateU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6B21CE2AD509D5FA220576E9DA5B3388DD9216BB (void);
// 0x00000014 System.Void BaseItemScript/<BaseItemAnimate>d__2::System.Collections.IEnumerator.Reset()
extern void U3CBaseItemAnimateU3Ed__2_System_Collections_IEnumerator_Reset_m90D6B24656FD76B0A4564BE65F1DBE4C7A446C9F (void);
// 0x00000015 System.Object BaseItemScript/<BaseItemAnimate>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CBaseItemAnimateU3Ed__2_System_Collections_IEnumerator_get_Current_m0427A930EEB4EFF0661A59991ABE3917427E69B7 (void);
// 0x00000016 System.Void TextureSheetAnimationScript::Awake()
extern void TextureSheetAnimationScript_Awake_mF1428B105F543D903409EBDCE4B0CE675CA7EA2F (void);
// 0x00000017 System.Void TextureSheetAnimationScript::Update()
extern void TextureSheetAnimationScript_Update_m3CDC4FBEA832CE3C93F0AF22CAE5C7E85453C9D4 (void);
// 0x00000018 System.Void TextureSheetAnimationScript::SetTextureSheetData(System.Int32,System.Int32,System.Int32,System.Single)
extern void TextureSheetAnimationScript_SetTextureSheetData_m3186488F9940E6EBA04AE1EE9659AA9464186D47 (void);
// 0x00000019 System.Void TextureSheetAnimationScript::SetFrame(System.Int32)
extern void TextureSheetAnimationScript_SetFrame_m2B58ECC8A33FCA70939B97D1B5F975AF6E9AD2B3 (void);
// 0x0000001A System.Void TextureSheetAnimationScript::.ctor()
extern void TextureSheetAnimationScript__ctor_mE312812C288CB9B227D57587344A928CA520EDD3 (void);
// 0x0000001B UnityEngine.GameObject Utilities::CreateInstance(UnityEngine.GameObject,UnityEngine.GameObject,System.Boolean)
extern void Utilities_CreateInstance_m578A81BB94EB3780DAB5C6E756FD5ADE76F2CF3C (void);
// 0x0000001C RenderQuadScript Utilities::CreateRenderQuad()
extern void Utilities_CreateRenderQuad_m2924CC9FA3266E366E80622E468969437376AFEB (void);
// 0x0000001D System.Void Utilities::.ctor()
extern void Utilities__ctor_mCC5CF7FD2F0CE04DBFC7E8F8A22F57C1C1016312 (void);
static Il2CppMethodPointer s_methodPointers[29] = 
{
	CameraManager_Start_m3A8F66B836B9A9BC419B62EF88451549BBC9CCD0,
	CameraManager_Update_m911D2029A80BF20148CD94D496452F232343FF60,
	CameraManager__ctor_m932EDBCBAE89115380AA929ED5BC9823B9371185,
	RenderQuadScript__ctor_mCAD65F9788F7DC64CDA65DB90713D5E111BCF086,
	SceneManager_Awake_m913FE5962F5FB4BFA11D0ED31212C66272B535E4,
	SceneManager_AddItem_m2EC2EF47336C47D92383227D2A8940A6547AC1EE,
	SceneManager__ctor_m50993E4C2BC958CFCF84678FE3A8304E1CF795BC,
	BaseItemRenderScript_Start_m72849706427F1D143ABBA0C8A05749F38D742810,
	BaseItemRenderScript_Update_mB3A9D29E6E1D43343BCF052E97D4A98A7C87E25E,
	BaseItemRenderScript_Refresh_m877A009F63B00372E11DE6CA073B9F12AF256B54,
	BaseItemRenderScript_UpdateRenderQuads_mD2A48EB5EA42A0153B300148A848ACA2383D1BC5,
	BaseItemRenderScript__ctor_mA055442301A5DFD64A2897876BB449D9CB507648,
	BaseItemScript_Start_m758933F1DECDB818C38180DAEC85C29A793E9435,
	BaseItemScript_BaseItemAnimate_m20AC0862D30741979D9369B02F446132A2DBB0F5,
	BaseItemScript__ctor_m239BA9844D8CE5A59EB8352D7736F3D96FD15E90,
	U3CBaseItemAnimateU3Ed__2__ctor_mF437DE2BCFA82602EB99858C6E20DB8A7D943247,
	U3CBaseItemAnimateU3Ed__2_System_IDisposable_Dispose_m1E960114DE81435DAC4FC0FAFC301D82F0394D9F,
	U3CBaseItemAnimateU3Ed__2_MoveNext_m6C90236A022D18B5F71181DC6784B81421383967,
	U3CBaseItemAnimateU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6B21CE2AD509D5FA220576E9DA5B3388DD9216BB,
	U3CBaseItemAnimateU3Ed__2_System_Collections_IEnumerator_Reset_m90D6B24656FD76B0A4564BE65F1DBE4C7A446C9F,
	U3CBaseItemAnimateU3Ed__2_System_Collections_IEnumerator_get_Current_m0427A930EEB4EFF0661A59991ABE3917427E69B7,
	TextureSheetAnimationScript_Awake_mF1428B105F543D903409EBDCE4B0CE675CA7EA2F,
	TextureSheetAnimationScript_Update_m3CDC4FBEA832CE3C93F0AF22CAE5C7E85453C9D4,
	TextureSheetAnimationScript_SetTextureSheetData_m3186488F9940E6EBA04AE1EE9659AA9464186D47,
	TextureSheetAnimationScript_SetFrame_m2B58ECC8A33FCA70939B97D1B5F975AF6E9AD2B3,
	TextureSheetAnimationScript__ctor_mE312812C288CB9B227D57587344A928CA520EDD3,
	Utilities_CreateInstance_m578A81BB94EB3780DAB5C6E756FD5ADE76F2CF3C,
	Utilities_CreateRenderQuad_m2924CC9FA3266E366E80622E468969437376AFEB,
	Utilities__ctor_mCC5CF7FD2F0CE04DBFC7E8F8A22F57C1C1016312,
};
static const int32_t s_InvokerIndices[29] = 
{
	846,
	846,
	846,
	846,
	846,
	444,
	846,
	846,
	846,
	724,
	724,
	846,
	846,
	824,
	846,
	724,
	846,
	840,
	824,
	846,
	824,
	846,
	846,
	183,
	724,
	846,
	1149,
	1476,
	846,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	29,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
