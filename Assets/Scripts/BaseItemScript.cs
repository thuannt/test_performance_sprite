using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseItemScript : MonoBehaviour
{
    public BaseItemRenderScript Renderer;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(BaseItemAnimate());
    }

    IEnumerator BaseItemAnimate()
    {
        this.Renderer.Refresh(0);
        yield return new WaitForSeconds(10f);
        this.Renderer.Refresh(1);
        yield return new WaitForSeconds(10f);
        this.Renderer.Refresh(2);
        yield return new WaitForSeconds(10f);
        this.Renderer.Refresh(3);
        yield return new WaitForSeconds(10f);
    }
}
