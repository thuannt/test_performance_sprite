using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseItemRenderScript : MonoBehaviour
{
    public GameObject RenderQuadsContainer;
    public Material[] QuadMaterials;
    private RenderQuadScript renderQuadInstance;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Refresh(int animateIndex)
    {
        this.UpdateRenderQuads(animateIndex);
    }

    public void UpdateRenderQuads(int index)
    {
        if (renderQuadInstance != null)
        {
            Destroy(renderQuadInstance.GetComponent<TextureSheetAnimationScript>());
            Destroy(renderQuadInstance.gameObject);
        }

        renderQuadInstance = Utilities.CreateRenderQuad();
        renderQuadInstance.transform.SetParent(this.RenderQuadsContainer.transform);

        renderQuadInstance.MeshRenderer.material = QuadMaterials[index];

        Vector3 defaultImgSize = new Vector3(1.4142f, 1.4142f, 1.4142f) * 8 * 18 / 100.0f;
        float heightFactor = 2;

        float offsetX = (1.414f / 256.0f) * (-4) * 4;
        float offsetY = (1.414f / 256.0f) * 25 * 4;
        renderQuadInstance.transform.localPosition = new Vector3(offsetX, offsetY, 0);
        renderQuadInstance.transform.localRotation = Quaternion.Euler(Vector3.zero);
        renderQuadInstance.transform.localScale = new Vector3(defaultImgSize.x, defaultImgSize.x * heightFactor, 1);

        renderQuadInstance.GetComponent<TextureSheetAnimationScript>()
            .SetTextureSheetData(4, 2, 8, 10);
    }
}
